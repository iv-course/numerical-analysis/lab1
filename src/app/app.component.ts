import { Component } from '@angular/core';
import { FormValues } from './components/input-form/input-form.component';
import { AlgebraicEquation, Equation, EquationType } from './models/equation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly EquationType = EquationType;

  input: FormValues | undefined;

  onInputChanges(input: FormValues) {
    this.input = input;
  }

  isAlgebraic(equation: Equation): equation is AlgebraicEquation {
    return equation.type === EquationType.Algebraic;
  }
}
