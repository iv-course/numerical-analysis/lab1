export enum EquationType {
    Transcendental = 'Transcendental',
    Algebraic = 'Algebraic'
}

export enum Method {
    Secant = 'Secant method',
    SimplifiedNewtons = "Simplified Newton's method",
    FixedPointIteration = 'Fixed-point iteration'
}

export type Equation = {
    plainText: string;
    type: EquationType;
    func: (x: number) => number;
}

export class AlgebraicEquation implements Equation {
    readonly type = EquationType.Algebraic;
    readonly plainText = this.getPlainText();
    readonly func = this.getFunc();

    constructor(public coefficients: number[]) { }

    private getPlainText(): string {
        return this.coefficients.reduce(
            (equation, coeff, index) => equation + this.buildNextPart(coeff, index), ''
        )
    }

    private getFunc(): (x: number) => number {
        return (x) => this.coefficients.reduce(
            (sum, coeff, index) => sum + coeff * Math.pow(x, this.getHighestPow() - index), 0
        )
    }

    private getHighestPow(): number {
        return this.coefficients.length - 1;
    }

    private buildNextPart(coeff: number, index: number): string {
        if (coeff === 0 && index < this.getHighestPow()) {
            return '';
        } else if (coeff === 0) {
            return ' = 0';
        }

        if (index === 0) {
            return `${coeff} x^${this.getHighestPow() - index}`
        }

        let equation = '';

        if (coeff < 0) {
            equation += ` - ${-coeff}`;
        } else {
            equation += ` + ${coeff}`;
        }

        if (index === this.getHighestPow()) {
            return equation + ' = 0';
        }

        return equation + ` x^${this.getHighestPow() - index}`;
    }
}
