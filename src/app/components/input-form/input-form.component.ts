import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { Subject, takeUntil } from 'rxjs';
import { Equation, Method, EquationType, AlgebraicEquation } from 'src/app/models/equation';

export interface FormValues {
  equation: Equation;
  method: Method;
}

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit, OnDestroy {
  @Output() formValues = new EventEmitter<FormValues>();

  readonly equationTypes = Object.values(EquationType);
  readonly methods = Object.values(Method);

  readonly form = new FormGroup({
    equation: new FormControl('', [Validators.required]),
    method: new FormControl('', [Validators.required]),
  });

  equationsShown: ReadonlyArray<Equation> | undefined;

  private readonly allEquations: ReadonlyArray<Equation> = [
    new AlgebraicEquation([61, 494, 680, -636, -777, 420, 69, -16]),
    {
      plainText: 'x + cos y',
      type: EquationType.Transcendental,
      func: () => 0,
    }
  ];

  private readonly destroy = new Subject<void>();

  ngOnInit() {
    this.form.valueChanges.pipe(
      takeUntil(this.destroy),
    ).subscribe(value => {
      if (this.isValidValue(value)) {
        this.formValues.emit(value);
      }
    });
  }

  ngOnDestroy() {
    this.destroy.next();
    this.destroy.complete();
  }

  onEquationTypeSelected({ value }: MatRadioChange) {
    this.equationsShown = this.allEquations.filter(
      equation => equation.type === value
    )
  }

  private isValidValue(value: any): value is FormValues {
    if (!value) {
      return false;
    }
    return !!value.equation && !!value.method;
  }
}
