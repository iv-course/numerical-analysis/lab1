import { Component, Input } from '@angular/core';
import { AlgebraicEquation } from 'src/app/models/equation';

@Component({
  selector: 'app-graeffe-calculator',
  templateUrl: './graeffe-calculator.component.html',
  styleUrls: ['./graeffe-calculator.component.scss']
})
export class GraeffeCalculatorComponent {
  @Input() equation!: AlgebraicEquation;

}
