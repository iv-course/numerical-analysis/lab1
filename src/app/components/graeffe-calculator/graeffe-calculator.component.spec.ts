import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraeffeCalculatorComponent } from './graeffe-calculator.component';

describe('GraeffeCalculatorComponent', () => {
  let component: GraeffeCalculatorComponent;
  let fixture: ComponentFixture<GraeffeCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraeffeCalculatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GraeffeCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
